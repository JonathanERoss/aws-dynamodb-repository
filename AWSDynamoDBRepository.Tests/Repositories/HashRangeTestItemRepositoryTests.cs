using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using AWSDynamoDBRepository.Dao;
using AWSDynamoDBRepository.Models;
using AWSDynamoDBRepository.Repositories;
using NUnit.Framework;

namespace AWSDynamoDBRepository.Tests.Repositories
{
    /// <summary>
    ///     Tests for the HashRangeTestItem Repository
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    [ExcludeFromCodeCoverage]
    public class HashRangeTestItemRepositoryTests
    {
        /// <summary>
        ///     The Repository
        /// </summary>
        private HashRangeTestItemRepository _repository;

        /// <summary>
        ///     UserId for Testing
        /// </summary>
        private string _testUserId;

        /// <summary>
        ///     The Test Table
        /// </summary>
        private DynamoDbTable _testTable;

        [SetUp]
        public void Setup()
        {
            _testTable = new DynamoDbTable("AWSDynamoDBRepository-HashRangeTestItemRepositoryTests-" + Guid.NewGuid().ToString(), "UserId", "ItemId");
            _testTable.CreateTableAsync(1,1).Wait();

            _repository = new HashRangeTestItemRepository(_testTable);
            _testUserId = Guid.NewGuid().ToString();
        }

        [TearDown]
        public void TeardownAsync()
        {
            _testTable.DeleteTableAsync().Wait();
        }

        [Test]
        public async Task SaveItemAsync()
        {
            HashRangeTestItem item = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item For Testing"
            };
            await _repository.SaveItemAsync(item);

            HashRangeTestItem readItem = await _repository.GetItemAsync(_testUserId, item.ItemId);
            Assert.AreEqual(item, readItem);
        }

        [Test]
        public async Task UpdateItemAsync()
        {
            HashRangeTestItem item = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item For Testing"
            };
            await _repository.SaveItemAsync(item);

            HashRangeTestItem readItem = await _repository.GetItemAsync(_testUserId, item.ItemId);
            Assert.AreEqual(item, readItem);

            readItem.Content = "New Content";
            await _repository.SaveItemAsync(readItem);

            HashRangeTestItem updatedItem = await _repository.GetItemAsync(_testUserId, item.ItemId);
            Assert.AreNotEqual(item, updatedItem);
        }

        [Test]
        public async Task RetrieveAllAsync()
        {
            HashRangeTestItem itemA = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item A For Testing"
            };
            await _repository.SaveItemAsync(itemA);

            HashRangeTestItem itemB = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item B For Testing"
            };
            await _repository.SaveItemAsync(itemB);

            IEnumerable<HashRangeTestItem> items = await _repository.GetAllItemsAsync(_testUserId);
            var hashRangeTestItems = items.ToList();
            CollectionAssert.Contains(hashRangeTestItems, itemA);
            CollectionAssert.Contains(hashRangeTestItems, itemB);
        }

        [Test]
        public async Task DeleteItemAsync()
        {
            HashRangeTestItem itemA = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item A For Testing"
            };
            await _repository.SaveItemAsync(itemA);

            HashRangeTestItem itemB = new HashRangeTestItem()
            {
                UserId = _testUserId,
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item B For Testing"
            };
            await _repository.SaveItemAsync(itemB);

            await _repository.DeleteItemAsync(itemA.UserId, itemA.ItemId);

            IEnumerable<HashRangeTestItem> items = await _repository.GetAllItemsAsync(_testUserId);
            var hashRangeTestItems = items.ToList();
            CollectionAssert.DoesNotContain(hashRangeTestItems, itemA);
            CollectionAssert.Contains(hashRangeTestItems, itemB);
        }
    }
}