using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using AWSDynamoDBRepository.Dao;
using AWSDynamoDBRepository.Models;
using AWSDynamoDBRepository.Repositories;
using NUnit.Framework;

namespace AWSDynamoDBRepository.Tests.Repositories
{
    /// <summary>
    ///     Tests for the HashTestItem Repository
    /// </summary>
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    [ExcludeFromCodeCoverage]
    public class HashTestItemRepositoryTests
    {
        /// <summary>
        ///     The Repository
        /// </summary>
        private HashTestItemRepository _repository;

        /// <summary>
        ///     The Test Table
        /// </summary>
        private DynamoDbTable _testTable;

        [SetUp]
        public void Setup()
        {
            _testTable = new DynamoDbTable("AWSDynamoDBRepository-HashTestItemRepositoryTests-" + Guid.NewGuid().ToString(), "ItemId");
            _testTable.CreateTableAsync(1,1).Wait();

            _repository = new HashTestItemRepository(_testTable);
        }

        [TearDown]
        public void TeardownAsync()
        {
            _testTable.DeleteTableAsync().Wait();
        }

        [Test]
        public async Task SaveItemAsync()
        {
            HashTestItem item = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item For Testing"
            };
            await _repository.SaveItemAsync(item);

            HashTestItem readItem = await _repository.GetItemAsync(item.ItemId);
            Assert.AreEqual(item, readItem);
        }

        [Test]
        public async Task UpdateItemAsync()
        {
            HashTestItem item = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item For Testing"
            };
            await _repository.SaveItemAsync(item);

            HashTestItem readItem = await _repository.GetItemAsync(item.ItemId);
            Assert.AreEqual(item, readItem);

            readItem.Content = "New Content";
            await _repository.SaveItemAsync(readItem);

            HashTestItem updatedItem = await _repository.GetItemAsync(item.ItemId);
            Assert.AreNotEqual(item, updatedItem);
        }

        [Test]
        public async Task RetrieveAllAsync()
        {
            HashTestItem itemA = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item A For Testing"
            };
            await _repository.SaveItemAsync(itemA);

            HashTestItem itemB = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item B For Testing"
            };
            await _repository.SaveItemAsync(itemB);

            IEnumerable<HashTestItem> items = await _repository.GetAllItemsAsync();
            var hashTestItems = items.ToList();
            CollectionAssert.Contains(hashTestItems, itemA);
            CollectionAssert.Contains(hashTestItems, itemB);
        }

        [Test]
        public async Task DeleteItemAsync()
        {
            HashTestItem itemA = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item A For Testing"
            };
            await _repository.SaveItemAsync(itemA);

            HashTestItem itemB = new HashTestItem()
            {
                ItemId = Guid.NewGuid().ToString(),
                Content = "Item B For Testing"
            };
            await _repository.SaveItemAsync(itemB);

            await _repository.DeleteItemAsync(itemA.ItemId);

            IEnumerable<HashTestItem> items = await _repository.GetAllItemsAsync();
            var hashTestItems = items.ToList();
            CollectionAssert.DoesNotContain(hashTestItems, itemA);
            CollectionAssert.Contains(hashTestItems, itemB);
        }
    }
}