﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSDynamoDBRepository.Dao
{
    public class DynamoDbDao<T> : IDynamoDbDao<T> 
    {

        private readonly DynamoDbTable _ddbTable;

        /// <summary>
        ///     The Context for the DynamoDB Table
        /// </summary>
        private readonly IDynamoDBContext _ddbContext;

        /// <summary>
        ///     The DynamoDB DAO
        /// </summary>
        /// <param name="ddbTable">The DynamoDB Table</param>
        public DynamoDbDao(DynamoDbTable ddbTable)
        {
            _ddbTable = ddbTable;

            AWSConfigsDynamoDB.Context.TypeMappings[typeof(T)] = new Amazon.Util.TypeMapping(typeof(T), _ddbTable.Name);

            _ddbContext = new DynamoDBContext(
                new AmazonDynamoDBClient(_ddbTable.Region),
                new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 }
            );
        }

        /// <summary>
        ///     Get all items.
        /// </summary>
        public async Task<IEnumerable<T>> GetAllItemsAsync()
        {
            AsyncSearch<T> search = _ddbContext.ScanAsync<T>(new List<ScanCondition>());
            return await search.GetNextSetAsync();
        }

        /// <summary>
        ///     Get all items.
        /// </summary>
        /// <param name="rangeKey">The range key to retrieve items for</param>
        public async Task<IEnumerable<T>> GetAllItemsAsync(string rangeKey)
        {
            IEnumerable<ScanCondition> conditions = new List<ScanCondition>();
            conditions.Append(new ScanCondition(_ddbTable.RangeKey, ScanOperator.Equal, rangeKey));

            AsyncSearch<T> search = _ddbContext.ScanAsync<T>(conditions);
            return await search.GetNextSetAsync();
        }

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        public async Task<T> GetItemAsync(string hashKey) 
            => await _ddbContext.LoadAsync<T>(hashKey);

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="rangeKey">The rangeKey of the item</param>
        public async Task<T> GetItemAsync(string hashKey, string rangeKey)
            => await _ddbContext.LoadAsync<T>(hashKey, rangeKey);

        /// <summary>
        ///     Save an item.
        /// </summary>
        /// <param name="item">The item to save</param>
        public async Task SaveItemAsync(T item)
            => await _ddbContext.SaveAsync(item);

        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        public async Task DeleteItemAsync(string hashKey) 
            => await _ddbContext.DeleteAsync<T>(hashKey);

        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="rangeKey">The rangeKey of the item</param>
        public async Task DeleteItemAsync(string hashKey, string rangeKey)
            => await _ddbContext.DeleteAsync<T>(hashKey, rangeKey);        
    }
}
