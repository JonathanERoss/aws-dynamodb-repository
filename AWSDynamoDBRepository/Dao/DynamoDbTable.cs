﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AWSDynamoDBRepository.Dao
{
    public class DynamoDbTable
    {
        /// <summary>
        ///     The Table Name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     The Property Name set as the Table Hash Key
        /// </summary>
        public string HashKey { get; private set; }

        /// <summary>
        ///     The Property Name set as the Table Range Key
        /// </summary>
        public string RangeKey { get; private set; }

        /// <summary>
        ///     The Region where the Table Exists
        /// </summary>
        public readonly RegionEndpoint Region = RegionEndpoint.USEast1;

        /// <summary>
        ///     The Client for the Table
        /// </summary>
        private AmazonDynamoDBClient Client;

        /// <summary>
        ///     DynamoDb Table Constructor
        /// </summary>
        /// <param name="name">The Table Name</param>
        /// <param name="hashKey">The Property Name set as the Table Hash Key</param>
        /// <param name="rangeKey">The Property Name set as the Table Range Key</param>
        
        public DynamoDbTable(string name, string hashKey, string rangeKey = null)
        {
            Name = name;
            HashKey = hashKey;
            RangeKey = rangeKey;

            Client = new AmazonDynamoDBClient(Region);
        }

        /// <summary>
        ///     Delete the Table
        /// </summary>
        public async Task DeleteTableAsync()
        {
            await Client.DeleteTableAsync(Name);
            Client.Dispose();
        }

        /// <summary>
        ///     Create the Table
        /// </summary>
        /// <param name="readCapacity">The Read Capacity</param>
        /// <param name="writeCapacity">The Write Capacity</param>
        public async Task CreateTableAsync(int readCapacity = 5, int writeCapacity = 5)
        {
            CreateTableRequest request = GetTableRequest(readCapacity, writeCapacity);

            await Client.CreateTableAsync(request);

            var describeRequest = new DescribeTableRequest { TableName = Name };
            DescribeTableResponse response = null;
            do
            {
                Thread.Sleep(1000);
                response = await Client.DescribeTableAsync(describeRequest);
            } while (response.Table.TableStatus != TableStatus.ACTIVE);
        }

        /// <summary>
        ///     Get a Table Request
        /// </summary>
        /// <param name="readCapacity">The Read Capacity</param>
        /// <param name="writeCapacity">The Write Capacity</param>
        private CreateTableRequest GetTableRequest(int readCapacity, int writeCapacity)
        {
            CreateTableRequest request = new CreateTableRequest
            {
                TableName = Name,
                ProvisionedThroughput = new ProvisionedThroughput
                {
                    ReadCapacityUnits = readCapacity,
                    WriteCapacityUnits = writeCapacity
                },
                KeySchema = new List<KeySchemaElement>
                {
                    new KeySchemaElement
                    {
                        KeyType = KeyType.HASH,
                        AttributeName = HashKey
                    }
                },
                AttributeDefinitions = new List<AttributeDefinition>
                {
                    new AttributeDefinition
                    {
                        AttributeName = HashKey,
                        AttributeType = ScalarAttributeType.S
                    }
                }
            };

            if (RangeKey != null)
            {
                request.KeySchema.Add(new KeySchemaElement
                {
                    KeyType = KeyType.RANGE,
                    AttributeName = RangeKey
                });
                request.AttributeDefinitions.Add(new AttributeDefinition
                {
                    AttributeName = RangeKey,
                    AttributeType = ScalarAttributeType.S
                });
            }

            return request;
        }
    }
}