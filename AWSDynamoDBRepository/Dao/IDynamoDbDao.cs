﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSDynamoDBRepository.Dao
{
    public interface IDynamoDbDao<T>
    {
        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        Task DeleteItemAsync(string hashKey);

        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="rangeKey">The rangeKey of the item</param>
        Task DeleteItemAsync(string hashKey, string rangeKey);

        /// <summary>
        ///     Get all items.
        /// </summary>
        Task<IEnumerable<T>> GetAllItemsAsync();

        /// <summary>
        ///     Get all items.
        /// </summary>
        /// <param name="hashKey">The hash key to retrieve items for</param>
        Task<IEnumerable<T>> GetAllItemsAsync(string hashKey);

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        Task<T> GetItemAsync(string hashKey);

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="rangeKey">The rangeKey of the item</param>
        Task<T> GetItemAsync(string hashKey, string rangeKey);

        /// <summary>
        ///     Save an item.
        /// </summary>
        /// <param name="item">The item to save</param>
        Task SaveItemAsync(T item);
    }
}