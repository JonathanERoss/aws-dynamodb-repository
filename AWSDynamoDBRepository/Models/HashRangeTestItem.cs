﻿using Amazon.DynamoDBv2.DataModel;

namespace AWSDynamoDBRepository.Models
{
    public class HashRangeTestItem
    {
        [DynamoDBHashKey]
        [DynamoDBProperty("UserId")]
        public string UserId { get; set; }

        [DynamoDBRangeKey]
        [DynamoDBProperty("ItemId")]
        public string ItemId { get; set; }

        [DynamoDBProperty("Content")]
        public string Content { get; set; }

        /// <summary>
        ///     Override equals to compare one TestItem with another
        /// </summary>
        /// <param name="obj">The object we're compared to</param>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            HashRangeTestItem t = (HashRangeTestItem)obj;
            return UserId == t.UserId
                && ItemId == t.ItemId
                && Content == t.Content;
        }

        /// <summary>
        ///     Override the hashcode for a TestItem
        /// </summary>
        public override int GetHashCode()
        {
            string hash = UserId + ItemId;
            return hash.GetHashCode();
        }
    }
}