﻿using Amazon.DynamoDBv2.DataModel;

namespace AWSDynamoDBRepository.Models
{
    public class HashTestItem
    {
        [DynamoDBHashKey]
        [DynamoDBProperty("ItemId")]
        public string ItemId { get; set; }

        [DynamoDBProperty("Content")]
        public string Content { get; set; }

        /// <summary>
        ///     Override equals to compare one TestItem with another
        /// </summary>
        /// <param name="obj">The object we're compared to</param>
        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            HashTestItem t = (HashTestItem)obj;
            return ItemId == t.ItemId
                && Content == t.Content;
        }

        /// <summary>
        ///     Override the hashcode for a TestItem
        /// </summary>
        public override int GetHashCode()
        {
            string hash = ItemId;
            return hash.GetHashCode();
        }
    }
}