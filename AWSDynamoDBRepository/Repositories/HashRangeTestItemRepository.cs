﻿using AWSDynamoDBRepository.Dao;
using AWSDynamoDBRepository.Models;

namespace AWSDynamoDBRepository.Repositories
{
    public class HashRangeTestItemRepository : RepositoryBase<HashRangeTestItem>
    {
        /// <summary>
        ///     The HashRangeTestItem Repository
        /// </summary>
        /// <param name="ddbTable">The DynamoDB Table</param>
        public HashRangeTestItemRepository(DynamoDbTable ddbTable) : base(ddbTable)
        {
        }
    }
}
