﻿using AWSDynamoDBRepository.Dao;
using AWSDynamoDBRepository.Models;

namespace AWSDynamoDBRepository.Repositories
{
    public class HashTestItemRepository : RepositoryBase<HashTestItem>
    {
        /// <summary>
        ///     The HashTestItem Repository
        /// </summary>
        /// <param name="ddbTable">The DynamoDB Table</param>
        public HashTestItemRepository(DynamoDbTable ddbTable) : base(ddbTable)
        {
        }
    }
}
