﻿using AWSDynamoDBRepository.Dao;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AWSDynamoDBRepository.Repositories
{
    public abstract class RepositoryBase<T>
    {
        /// <summary>
        ///     The Database interface
        /// </summary>
        private readonly IDynamoDbDao<T> _dynamoDb;

        /// <summary>
        ///     Base Class for a repository
        /// </summary>
        /// <param name="ddbTable">The DynamoDB Table</param>
        protected RepositoryBase(DynamoDbTable ddbTable)
        {
            _dynamoDb = new DynamoDbDao<T>(ddbTable);
        }

        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        public Task DeleteItemAsync(string hashKey)
            => _dynamoDb.DeleteItemAsync(hashKey);

        /// <summary>
        ///     Delete an item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="sortKey">The sortKey of the item</param>
        public Task DeleteItemAsync(string hashKey, string sortKey)
            => _dynamoDb.DeleteItemAsync(hashKey, sortKey);

        /// <summary>
        ///     Get all items.
        /// </summary>
        /// <param name="hashKey">The hash key to retrieve items for</param>
        public Task<IEnumerable<T>> GetAllItemsAsync(string hashKey)
            => _dynamoDb.GetAllItemsAsync(hashKey);

        /// <summary>
        ///     Get all items.
        /// </summary>
        public Task<IEnumerable<T>> GetAllItemsAsync()
            => _dynamoDb.GetAllItemsAsync();

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        public Task<T> GetItemAsync(string hashKey)
            => _dynamoDb.GetItemAsync(hashKey);

        /// <summary>
        ///     Get a single item.
        /// </summary>
        /// <param name="hashKey">The hashKey of the item</param>
        /// <param name="sortKey">The sortKey of the item</param>
        public Task<T> GetItemAsync(string hashKey, string sortKey)
            => _dynamoDb.GetItemAsync(hashKey, sortKey);

        /// <summary>
        ///     Save an item.
        /// </summary>
        /// <param name="item">The item to save</param>
        public Task SaveItemAsync(T item)
            => _dynamoDb.SaveItemAsync(item);
    }
}
