# AWS DynamoDB Repository

Generic Repository for AWS DynamoDB Tables

## Overview
This application is a .NET Core 2.1 class library written in C#.  There are two projects. One for the class library and another for unit tests.

- AWSDynamoDBRepository
- AWSDynamoDBRepository.Tests

The purpose of this application is to provide an example of working with DynamoDB tables.  There are examples for tables that contain both Hash and Range Keys and for tables having only Hash Keys.

## Running The Tests

From the command line, run the following in the root of the repository:

    dotnet test .\AWSDynamoDBRepository.Tests\AWSDynamoDBRepository.Tests.csproj --verbosity normal

This will build the solution, then execute the tests.  You may also build using the following commands.

	dotnet clean
	dotnet build

The build step will fetch all nuget packages required of the projects.  It is not necessary, but to manually restore the packages simply execute the following command:

    dotnet restore

You may also open the solution file (AWSDynamoDBRepository.sln) with Visual Studio and run tests.

## Author
- Jonathan E. Ross
